﻿using FluentValidation;

namespace TestValidation.Models
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(user => user.Login).Matches("[A-Z][a-z]*");
        }
    }
}
